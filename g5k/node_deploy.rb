#!/usr/bin/ruby -w
require 'cute'

g5k = Cute::G5K::API.new()

sites = g5k.site_uids

sites.each{ |site|
        job = g5k.reserve(:nodes => 3, :site => 'nancy', :cluster => 'griffon', :walltime => '08:00:00', :type => :deploy)

        begin
                g5k.deploy(job,:env => 'jessie-x64-min')
                g5k.wait_for_deploy(job,:wait_time => 600)
                puts "Nodes assigned #{job['assigned_nodes']}"
                nodes =  job['assigned_nodes']
                fichier = 'hostnames.txt'
                fic = File.new(fichier, 'w')
                fic.puts(nodes)
                fic.close
                break
        rescue  Cute::G5K::EventTimeout
                puts "We waited too long let's release the job"
                g5k.release(job)
        end
}
