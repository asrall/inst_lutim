# Divers scripts de déploiement de noeuds Grid5000, et d'installation de Lutim

Ce dépôt continet plusieurs scripts, pour
- Déployer des nœeuds Grid5000 (node_deploy.rb)
- Installer Lutim, soit avec SQLite (g5k\_lutim.sh), soit avec PostgreSQL (g5k\_lutim-v2.sh, postgre-lutim.sh), avec divers outils à des fins de test, et de monitoring
- De configurer Lutim pour fonctionner avec PosgreSQL sur Grid5000)
- Installer Locust pour faire des tests de charges

Le dépôt contient également un Locustfile pour uploader une image sur une instance Lutim

### g5k_lutim-v2.sh

Ce script permet d'automatiser l'installation de Lutim sur Grid5000  
Il peut également utilisé pour installer Lutim sur des machines Debian hors Grid5000, à conditions de remplacer sudo-g5k par sudo (ou par rien) dans la 1ere version du script, et éventuellement rajouter -y à apt-get install (1)

vim :
:%s/sudo-g5k/sudo/g
:%s/apt-get install/apt-get -y install/g

(1) Les options des commandes (dont l'option "--assume-yes" de **apt**) ne sont pas reconnues quand on utilise **sudo-g5k**. et exécuter le script en tant que *root* nécessite de se déconnecter du compte *root* pour que le script puisse s'exécuter (au moment de la déconnexion). Ça ne limite donc pas l'interaction avec utilisateur du script, l'utilisation de "--assume-yes" n'apporte rien dans ce cas, à moins de trouver un workaround pour Grid5000.

La version 2 du script (g5k_lutim-v2.sh) s'utilise en root sur une Debian standard, après réinstallation d'une Debian sur Grid5000 (ou hors Grid5000) donc sans la restriction liée au wrapper sudo-g5k.
Pour mettre l'adresse ip du serveur PostgreSQL. Ouvrir le fichier ipPostgre.txt, remplacer "localhost" par votre adresse.