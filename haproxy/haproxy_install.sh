#!/bin/bash
#set -x

echo "deb http://httpredir.debian.org/debian jessie-backports main contrib non-free" >> /etc/apt/sources.list.d/backports

apt-get update
apt-get -t jessie-backports install haproxy

cd /etc/haproxy/
mv haproxy.cfg haproxy.cfg.old
wget -O haproxy.cfg https://framagit.org/asrall/inst_lutim/raw/master/haproxy/haproxy.cfg

echo "Nombre d'instances de Lutim ?"
read nb

x=$(( 100 / nb )) # Defines backends weight for loadbalancing, total is 100, since all backend nodes are similar (same hardware), weight is 100/nodes_number

for (( i=1; i<=$nb; i++ ))
do
	echo "Adresse IP de l'instance $i Lutim ?"
	read ip
	echo -e "\tserver lutim$i $ip:8080 weight $x cookie lutim$i check inter 1s" >> haproxy.cfg	
done
systemctl restart haproxy
