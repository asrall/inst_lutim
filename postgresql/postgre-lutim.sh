#!/bin/bash
#set -x

apt-get update
apt-get install postgresql

useradd -m lutim
echo "Mot de passe lutim"
passwd lutim

su postgres -c "psql -c 'CREATE ROLE lutim WITH CREATEDB;'"

su postgres -c "psql -c 'ALTER ROLE lutim WITH LOGIN;'"

su lutim -c "createdb lutimDB"

echo "Nombre d' instance Lutim ?"
read nb

for (( i=1; i <= $nb; i++))
do		
	echo "Adresse IP de l'instance $i lutim ?"
	read ip
	echo "host all lutim $ip/24 trust" >> /etc/postgresql/9.*/main/pg_hba.conf
done

sed -i -e "s/#listen_addresses = '.*'/listen_addresses = '*'/g" /etc/postgresql/9.*/main/postgresql.conf


