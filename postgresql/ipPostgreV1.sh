#!/bin/bash

value=`cat ipPostgre.txt`

sed -i -e "s/postgresql:\/\/.*\/lutimDB/postgresql:\/\/$value\/lutimDB/g" lutim/lib/Lutim.pm
sed -i -e "s/postgresql:\/\/.*\/lutimDB/postgresql:\/\/$value\/lutimDB/g" lutim/lib/LutimModel.pm
sed -i -e "s/postgresql:\/\/.*\/lutimDB/postgresql:\/\/$value\/lutimDB/g" lutim/lib/Lutim/Command/cron/cleanfiles.pm
sed -i -e "s/postgresql:\/\/.*\/lutimDB/postgresql:\/\/$value\/lutimDB/g" lutim/lib/Lutim/Command/cron/cleanbdd.pm
sed -i -e "s/postgresql:\/\/.*\/lutimDB/postgresql:\/\/$value\/lutimDB/g" lutim/lib/Lutim/Command/cron/stats.pm
sed -i -e "s/postgresql:\/\/.*\/lutimDB/postgresql:\/\/$value\/lutimDB/g" lutim/lib/Lutim/Controller.pm
