from locust import HttpLocust, TaskSet

def index(self):
    self.client.get("/")

def post_img(self):
    headers = {'1': '1', '2': '2'}
    test_file = '/path/to/file.jpg'
    self.client.request('POST', '/', files={'file': open(test_file, 'rb')}, headers=headers)

class UserBehavior(TaskSet):
    tasks = {index:1,post_img:2} 

class MyLocust(HttpLocust):
    task_set = UserBehavior
    min_wait = 1000
    max_wait = 5000
