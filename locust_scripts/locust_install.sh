#!/bin/bash
#set -x #Debugging

apt-get update
apt-get install virtualenvwrapper build-essential python-dev python-pip
source /usr/share/virtualenvwrapper/virtualenvwrapper.sh
mkvirtualenv locust
pip install locustio

export PATH=$PATH:.virtualenvs/locust/bin
wget -O ~/.virtualenvs/locust/locustfile.py https://framagit.org/asrall/inst_lutim/raw/master/locust_scripts/locustfile.py