#!/bin/bash
#set -x #Debugging
start=y #Lancement de Lutim, Oui par défaut

apt-get update
apt-get install build-essential libssl-dev shared-mime-info git libpq-dev htop nmon slurm tmux vim file
cpan Carton

echo "Carton bien installé ? [y|n]"
read rep

if [ $rep == "n" ]
then
	apt-get install carton
fi

useradd -m lutim
echo "Mot de passe lutim"
passwd lutim
cd /home/lutim
su lutim -c "git clone https://framagit.org/Lenny/lutim.git"
cd lutim

rm -rf local/
carton install

sed -e "s/rel_dir/rel_file/g" -i local/lib/perl5/Mojolicious/Plugin/AssetPack.pm

echo "Entrez l'adresse ip du serveur postgreSQL !"
read ip

sed -i -e "s/postgresql:\/\/.*\/lutimDB/postgresql:\/\/$ip\/lutimDB/g" lib/Lutim.pm
sed -i -e "s/postgresql:\/\/.*\/lutimDB/postgresql:\/\/$ip\/lutimDB/g" lib/LutimModel.pm
sed -i -e "s/postgresql:\/\/.*\/lutimDB/postgresql:\/\/$ip\/lutimDB/g" lib/Lutim/Command/cron/cleanfiles.pm
sed -i -e "s/postgresql:\/\/.*\/lutimDB/postgresql:\/\/$ip\/lutimDB/g" lib/Lutim/Command/cron/cleanbdd.pm
sed -i -e "s/postgresql:\/\/.*\/lutimDB/postgresql:\/\/$ip\/lutimDB/g" lib/Lutim/Command/cron/stats.pm
sed -i -e "s/postgresql:\/\/.*\/lutimDB/postgresql:\/\/$ip\/lutimDB/g" lib/Lutim/Controller.pm

su lutim

#Décommentez les 2 lignes suivantes pour pouvoir installer Lutim en choisissant
#si on veut le lancer ou non (configuration supplémentaire avant la première exécution par exemple)
#echo "Voulez vous lancez Lutim? [y|n]"
#read start
#if [ $start == "y" ]
#then
#       echo "Lancement de Lutim"
#       carton exec hypnotoad script/lutim
#else
#       echo "Lutim ne sera pas lancé automagiquement"
#fi
#
exit 0
