#!/bin/bash
#set -x #Debugging
inter=0 #Nom de l'interface
rep=0 #Confirmation de l'adresse IP à mettre dans le fichier lutim.conf
start=y #Lancement de Lutim, Oui par défaut

apt-get update
apt-get install build-essential libssl-dev shared-mime-info git nmon w3m

cpan Carton

git clone https://framagit.org/luc/lutim.git
cd lutim
carton install
cp lutim.conf.template lutim.conf
sed -i "s/\#contact/contact/" lutim.conf #décommentez la ligne contact (sans mettre un vrai contact pour le moment)

ip -4 a #Pour spécifier une IPv4 seulement
#Il faudra adapter le script si on veut utiliser IPv6 : 'http://[::]:port' dans lutim.conf
#'http://*:port' pour écouter sur n'importe quelle IPv4… (voir doc Mojolicious)
echo "Entrez le nom de l'interface que vous voulez utiliser :"
read inter

#On récupère l'IPv4 pour le fichier de configuration
export IP=`ip -4 addr show $inter | tail -n2 | head -n1 | cut -d ' ' -f6`

#On retire le masque CIDR (3 derniers caractères)
export IP=${IP::-3}

echo "Votre adresse IP est $IP, vous confirmez? [y|n]"
read rep
if  [ $rep == "n" ] || [ $rep == "N" ]
then
         echo "Vérifier l'extraction de l'IP sur votre distribution avant de recommencer"
         exit 2
elif [ $rep == "y" ] || [ $rep == "Y" ]
then
        sed -i "s/127.0.0.1/$IP/" lutim.conf
fi

#Décommentez les 2 lignes suivantes pour pouvoir installer Lutim en choisissant
#si on veut le lancer ou non (configuration supplémentaire avant la première exécution par exemple)
#echo "Voulez vous lancez Lutim? [y|n]"
#read start
if [ $start == "y" ]
then
       echo "Lancement de Lutim"
       carton exec hypnotoad script/lutim
else
       echo "Lutim ne sera pas lancé automagiquement"
fi

exit 0